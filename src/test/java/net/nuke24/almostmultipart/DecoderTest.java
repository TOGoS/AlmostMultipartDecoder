package net.nuke24.almostmultipart;

import org.junit.Test;
import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class DecoderTest {
	static class Chunk {
		public final HashMap<String, String> metadata;
		public final byte[] data;
		public Chunk(HashMap<String, String> metadata, byte[] data) {
			this.metadata = metadata;
			this.data = data;
		}
		@Override public boolean equals(Object other) {
			if( !(other instanceof Chunk) ) return false;
			Chunk oc = (Chunk)other;
			if( this.metadata.size() != oc.metadata.size() ) return false;
			for( Map.Entry<String,String> entry : this.metadata.entrySet() ) {
				if( !entry.getValue().equals(oc.metadata.get(entry.getKey())) ) return false;
			}
			return Arrays.equals(this.data, oc.data);
		}
		@Override public String toString() {
			StringBuilder sb = new StringBuilder();
			for( Map.Entry<String,String> md : this.metadata.entrySet() ) {
				sb.append(md.getKey()+": "+md.getValue()+"\n");
			}
			sb.append("\n");
			try {
				sb.append(Util.utf8Decode(this.data));
			} catch( MalformedInputException e ) {
				sb.append("("+this.data+" bytes of data)");
			}
			return sb.toString();
		}
	}
	
	static Chunk makeTestChunk(String filename, String data) {
		HashMap<String,String> md = new HashMap<>();
		md.put("filename", filename);
		return new Chunk(md, Util.utf8(data));
	}
	
	static class ChunkCollector implements Decoder.ChunkHandler {
		HashMap<String,String> chunkMetadata;
		ByteArrayOutputStream chunkData;
		
		public ArrayList<Chunk> chunks = new ArrayList<>();
		
		@Override
		public void parseWarning(String message) {
			System.err.println("Parse warning: "+message);
		}
		
		@Override
		public void streamMetadata(String k, String v) {
		}
		
		@Override
		public void openChunk() {
			this.chunkMetadata = new HashMap<>();
			this.chunkData = new ByteArrayOutputStream();
		}
		
		@Override
		public void chunkMetadata(String k, String v) {
			chunkMetadata.put(k, v);
		}
		
		@Override
		public void chunkData(byte[] data, int offset, int len) {
			chunkData.write(data, offset, len);
		}
		
		@Override
		public void closeChunk() {
			this.chunks.add(new Chunk(chunkMetadata, chunkData.toByteArray()));
			this.chunkMetadata = null;
			this.chunkData = null;
		}
		
		@Override
		public void close() {
			this.chunkMetadata = null;
			this.chunkData = null;
		}
	}
	
	static final void feed(Decoder d, String input) throws IOException, MalformedInputException {
		byte[] dataBytes = Util.utf8(input);
		d.input(dataBytes, 0, dataBytes.length);
	}
	static final ArrayList<Chunk> decode(String input) throws IOException, MalformedInputException {
		try( ChunkCollector cc = new ChunkCollector(); Decoder dec = new Decoder(cc) ) {
			feed(dec, input);
			return cc.chunks;
		}
	}
	
	@Test
	public void testDecodeOneChunk() throws IOException, MalformedInputException {
		var chunks = decode(
			"content-type: x-almost-multipart/mixed; boundary=abc123\n" +  // Bytes  0-17
			"\n" +                  // Bytes 17-18
			"--abc123\n" +          // Bytes 18-27
			"filename: foo.txt\n" + // Bytes 27-46
			"\n" +                  // Bytes 46-47
			"Hello, world!\n" +     // Bytes 47-61
			"--abc123--\n" +        // Bytes 61-72
			"epilogue, which is ignored\n"
		);
		
		assertEquals(1, chunks.size());
		assertEquals(makeTestChunk("foo.txt", "Hello, world!"), chunks.get(0));
	}
	
	@Test
	public void testDecodeEmptyChunk() throws IOException, MalformedInputException {
		var chunks = decode(
			"boundary: abc123\n" + // Bytes  0-17
			"\n" +
			"--abc123\n" +
			"filename: foo.txt\n" +
			"\n" +
			"--abc123--\n" +
			"epilogue, which is ignored\n"
		);
		
		assertEquals(1, chunks.size());
		assertEquals(makeTestChunk("foo.txt", ""), chunks.get(0));
	}
	
	@Test
	public void testDecodeTwoChunks() throws IOException, MalformedInputException {
		var chunks = decode(
			"Content-Type: x-almost-multipart/mixed; boundary=abc123\n" +
			"\n" +
			"--abc123\n" +
			"filename: foo.txt\n" +
			"\n" +
		   "--abc123\n" +
		   "filename: bar.txt\n" +
		   "\n" +
		   "Bar bar bar.\n" +
		   "\n" +
			"--abc123--\n" +
			"epilogue, which is ignored\n"
		);
		
		assertEquals(2, chunks.size());
		assertEquals(makeTestChunk("foo.txt", ""), chunks.get(0));
		assertEquals(makeTestChunk("bar.txt", "Bar bar bar.\n"), chunks.get(1));
	}
	
	//
	
	protected static String generateInput(String contentTypeBase, String nl) {
		return
			"Content-Type: "+contentTypeBase+"/mixed; boundary=abc123" + nl +
			nl +
			"--abc123" +
			"filename: foo.txt" + nl +
			nl +
			"Hello, world!" + nl +
			"--abc123--" + nl +
			"epilogue, which is ignored" + nl;
	}
	
	@Test(expected = MalformedInputException.class)
	public void testErrorWhenCrLfs() throws IOException, MalformedInputException {
		decode(generateInput("x-almost-multipart", "\r\n"));
	}
	
	@Test(expected = MalformedInputException.class)
	public void testErrorWhenWrongContentTypeAndCrLfs() throws IOException, MalformedInputException {
		decode(generateInput("multipart", "\r\n"));
	}
	
	@Test(expected = MalformedInputException.class)
	public void testErrorWhenWrongContentType() throws IOException, MalformedInputException {
		decode(generateInput("multipart", "\n"));
	}
	
	@Test(expected = MalformedInputException.class)
	public void testErrorWhenUnterminatedChunks() throws IOException, MalformedInputException {
		String nl = "\n";
		decode(
			"Content-Type: x-almost-multipart/mixed; boundary=abc123" + nl +
			nl +
			"--abc123" +
			"filename: foo.txt" + nl +
			nl +
			"Hello, world!" + nl
		);
	}
	
	@Test(expected = MalformedInputException.class)
	public void testErrorWhenInvalidBoundarySuffix() throws IOException, MalformedInputException {
		String nl = "\n";
		decode(
			"Content-Type: x-almost-multipart/mixed; boundary=abc123" + nl +
			nl +
			"--abc123x" + // Expect '\n' or '-', not 'x'
			"filename: foo.txt" + nl +
			nl +
			"Hello, world!" + nl +
			"--abc123--" + nl
		);
	}
	
	@Test
	public void testSgOutput() throws IOException {
		ArrayList<Chunk> chunks = decode(
			"content-type: x-almost-multipart/mixed; boundary=Qgu21hbYvN06qzgDJJYhPoSaeYSlOR\n" +
			"\n" +
			"\n" +
			"--Qgu21hbYvN06qzgDJJYhPoSaeYSlOR\n" +
			"filename: vcv-plugins/SG2100P0002SimpleOscPlugin/src/plugin.cpp\n" +
			"\n" +
			"#include \"plugin.hpp\"\n" +
			"\n" +
			"--Qgu21hbYvN06qzgDJJYhPoSaeYSlOR\n" +
			"filename: vcv-plugins/SG2100P0002SimpleOscPlugin/plugin.json\n" +
			"\n" +
			"{\n" +
			"\t\"slug\": \"SG2100P0002SimpleOscPlugin\"\n" +
			"}\n" +
			"--Qgu21hbYvN06qzgDJJYhPoSaeYSlOR--\n" +
			"\n" +
			"That was the source code for a VCV plugin.\n" +
			"Blah blah\n"
		);
		assertEquals(2, chunks.size());
		assertEquals("vcv-plugins/SG2100P0002SimpleOscPlugin/src/plugin.cpp", chunks.get(0).metadata.get("filename") );
		assertEquals("#include \"plugin.hpp\"\n", new String(chunks.get(0).data));
		assertEquals("vcv-plugins/SG2100P0002SimpleOscPlugin/plugin.json", chunks.get(1).metadata.get("filename") );
		assertEquals("{\n\t\"slug\": \"SG2100P0002SimpleOscPlugin\"\n}", new String(chunks.get(1).data));
	}
}
