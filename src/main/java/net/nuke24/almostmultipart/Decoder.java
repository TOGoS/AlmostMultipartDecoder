package net.nuke24.almostmultipart;

import javafx.css.Rule;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Decoder implements AutoCloseable {
	static final String UNSET_CONTENT_TYPE_BASE = "(unset)";
	static final byte[] UNSET_SEQUENCE = Util.utf8("\n--UNINITIALIZED-BOUNDARY-SEQUENCE");
	static final String X_ALMOST_MULTIPART = "x-almost-multipart";
	
	interface ChunkHandler extends AutoCloseable {
		void parseWarning(String message);
		void streamMetadata(String k, String v) throws IOException;
		void openChunk() throws IOException;
		void chunkMetadata(String k, String v) throws IOException;
		void chunkData(byte[] data, int offset, int len) throws IOException;
		void closeChunk() throws IOException;
		void close();
	}
	
	protected final ChunkHandler chunkHandler;
	public Decoder(ChunkHandler chunkHandler) {
		this.chunkHandler = chunkHandler;
	}
	
	enum State {
		STREAM_HEADER,
		PREAMBLE, // Anything after the stream headers
		BOUNDARY_READ, // Now we're reading the post-boundary sequence; either "--\n" or "\n"
		CHUNK_HEADER,
		CHUNK_BODY,
		EPILOGUE, // Everything after the last --boundary--
		EOF,
	}
	State state = State.STREAM_HEADER;
	// Boundary sequence, including newline, dash dash, indicated boundary string
	protected String contentTypeBase = UNSET_CONTENT_TYPE_BASE;
	protected byte[] boundarySequence = UNSET_SEQUENCE;
	int dataStartWithinBoundarySequence = 0;
	int positionWithinBoundarySequence = 0;
	ByteArrayOutputStream currentHeader = new ByteArrayOutputStream();
	
	static final byte[] mkBoundarySequence(String indicatedPart) {
		return Util.utf8("\n--"+indicatedPart.trim());
	}
	
	static final Pattern CTPAT = Pattern.compile("(?x) ^ ([^/]+) / ([^/]+) (?: \\s* ; \\s* boundary=([^\\s]+))");
	
	protected void streamHeader(String k, String v) throws IOException {
		if( "boundary".equals(k) ) {
			contentTypeBase = "x-almost-multipart";
			boundarySequence = mkBoundarySequence(v.trim());
		} else if( "content-type".equals(k) ) {
			Matcher ctm = CTPAT.matcher(v);
			if( ctm.matches() ) {
				contentTypeBase = ctm.group(1);
				if( ctm.group(3) != null ) boundarySequence = mkBoundarySequence(ctm.group(3));
			}
		}
		chunkHandler.streamMetadata(k, v);
	}
	
	Pattern HEADER_REGEX = Pattern.compile("^(.*?): +(.*)$");
	protected void streamHeader(String line) throws IOException {
		Matcher m = HEADER_REGEX.matcher(line);
		if( !m.matches() ) {
			chunkHandler.parseWarning("Malformed stream header: "+line);
		}
		streamHeader(m.group(1).toLowerCase(), m.group(2));
	}
	
	protected void chunkHeader(String line) throws IOException {
		Matcher m = HEADER_REGEX.matcher(line);
		if( !m.matches() ) {
			chunkHandler.parseWarning("Malformed chunk header: "+line);
		}
		chunkHandler.chunkMetadata(m.group(1).toLowerCase(), m.group(2));
	}
	
	byte[] dataBuffer = new byte[1024];
	int dataBufferPos = 0;
	protected void clearData() {
		dataBufferPos = 0;
	}
	protected void dataByte(byte b) throws IOException {
		dataBuffer[dataBufferPos] = b;
		++dataBufferPos;
		if( dataBufferPos == dataBuffer.length ) {
			flushData();
		}
	}
	protected void flushData() throws IOException {
		switch( this.state ) {
			case PREAMBLE:
			case EPILOGUE:
				break;
				// Do nothing!
			case CHUNK_BODY:
				if( dataBufferPos > 0 ) {
					this.chunkHandler.chunkData(dataBuffer, 0, dataBufferPos);
					dataBufferPos = 0;
				}
				break;
			default:
				throw new RuntimeException("Nonsensical state in which to flushData(): "+this.state);
		}
	}
	
	protected void setState(State s) throws IOException, MalformedInputException {
		assert(s != this.state);
		
		switch( this.state ) {
			case CHUNK_BODY:
				flushData();
				chunkHandler.closeChunk();
				break;
		}
		
		switch(s) {
			case PREAMBLE:
				this.state = State.PREAMBLE;
				break;
			case CHUNK_HEADER:
				this.state = State.CHUNK_HEADER;
				this.chunkHandler.openChunk();
				break;
			case CHUNK_BODY:
				this.state = State.CHUNK_BODY;
				clearData();
				break;
			case BOUNDARY_READ:
			case EPILOGUE:
				this.state = s;
				break;
			case EOF:
				switch(this.state) {
					case PREAMBLE: case EPILOGUE:
						break;
					default:
						throw new MalformedInputException("EOF encountered while in state: "+this.state);
				}
				this.state = State.EOF;
				break;
		}
		assert(s == this.state);
	}
	
	protected void validateType() throws IOException, MalformedInputException {
		if( boundarySequence == UNSET_SEQUENCE ) {
			throw new MalformedInputException("Boundary sequence not set");
		}
		if( contentTypeBase == UNSET_CONTENT_TYPE_BASE ) {
			contentTypeBase = X_ALMOST_MULTIPART;
		}
		if( !X_ALMOST_MULTIPART.equals(contentTypeBase) ) {
			throw new MalformedInputException("Content type '" + contentTypeBase + "' not supported (only "+X_ALMOST_MULTIPART+" is)");
		}
	}
	
	protected void endHeaders(State newState) throws IOException, MalformedInputException {
		if( state == State.STREAM_HEADER ) {
			validateType();
		}
		
		setState(newState);
		dataStartWithinBoundarySequence = 1; // Because even if this \n marks the beginning of the boundary,
		                                     // It was *not* part of the data!
		positionWithinBoundarySequence = 1; // Because this \n might actually be the first of the sequence!
	}
	
	void handlePossibleBoundaryByte(byte dat, boolean emitDataByte) throws IOException {
		while(true) {
			if (dat == boundarySequence[positionWithinBoundarySequence]) {
				if (++positionWithinBoundarySequence == boundarySequence.length) {
					// We found boundary!
					setState(State.BOUNDARY_READ);
					dataStartWithinBoundarySequence = 0;
					positionWithinBoundarySequence = 0;
				}
				break;
			} else if( positionWithinBoundarySequence == 0 ) {
				// Definitely not a boundary byte, just emit it!
				if( emitDataByte ) dataByte(dat);
				break;
			} else {
				// Write any boundary bytes that we've been not writing
				for (int i = dataStartWithinBoundarySequence; i < positionWithinBoundarySequence; ++i) {
					if( emitDataByte ) dataByte(boundarySequence[i]);
				}
				dataStartWithinBoundarySequence = 0;
				positionWithinBoundarySequence = 0;
				// And then we check again, since this could be the start of a boundary!
			}
		}
	}
	
	int bytesProcessed = 0;
	public void input(byte dat) throws IOException, MalformedInputException {
		switch (this.state) {
			case STREAM_HEADER:
				if (dat == '\r') {
					throw new MalformedInputException("Carriage returns in headers not supported!");
				} else if (dat == '\n') {
					if (currentHeader.size() == 0) {
						endHeaders(State.PREAMBLE);
					} else {
						streamHeader(Util.utf8Decode(currentHeader.toByteArray()));
					}
					currentHeader = new ByteArrayOutputStream();
				} else {
					currentHeader.write(dat);
				}
				break;
			case CHUNK_HEADER:
				if (dat == '\r') {
					throw new MalformedInputException("CRs not allowed");
				} else if (dat == '\n') {
					if (currentHeader.size() == 0) {
						endHeaders(State.CHUNK_BODY);
					} else {
						chunkHeader(Util.utf8Decode(currentHeader.toByteArray()));
					}
					currentHeader = new ByteArrayOutputStream();
				} else {
					currentHeader.write(dat);
				}
				break;
			case PREAMBLE:
				handlePossibleBoundaryByte(dat, false);
				break;
			case CHUNK_BODY:
				handlePossibleBoundaryByte(dat, true);
				break;
			case BOUNDARY_READ:
				switch (dat) {
					case '\n':
						setState(State.CHUNK_HEADER);
						break;
					case '-':
						// That was the last chunk!  Ignore everything else.
						setState(State.EPILOGUE);
						break;
					default:
						throw new MalformedInputException("Expected dash or newline after boundary, but got '" + (char) dat + "' (byte " + dat + ")");
				}
		}
		++bytesProcessed;
	}
	
	public void input(byte[] data, int offset, int len) throws IOException, MalformedInputException {
		for( int i=0; i<len; ++i ) {
			input(data[offset+i]);
		}
	}
	@Override
	public void close() throws IOException, MalformedInputException {
		setState(State.EOF);
	}
	
	enum RunMode {
		UNSET,
		HELP,
		EXTRACT,
		LIST,
	}
	
	static class ExtractionRule {
		public final String sourcePath;
		public final String destPath;
		ExtractionRule(String sourcePath, String destPath) {
			if( sourcePath.isEmpty() ) throw new RuntimeException("Empty source path; use '.' to indicate root of archive");
			if( destPath.isEmpty() ) throw new RuntimeException("Empty destination path; use '.' to indicate current directory");
			this.sourcePath = sourcePath;
			this.destPath = destPath;
		}
	}
	
	static class ExtractionRuleChunkHandler implements ChunkHandler {
		protected final ArrayList<ExtractionRule> extractionRules;
		/** Null unless within a part */
		protected ArrayList<OutputStream> currentOutputs = null;
		public ExtractionRuleChunkHandler(ArrayList<ExtractionRule> extractionRules) {
			this.extractionRules = extractionRules;
		}
		
		@Override
		public void parseWarning(String w) {
			System.err.println("warning: "+w);
		}
		
		@Override
		public void streamMetadata(String k, String v) {}
		
		@Override
		public void openChunk() {
			currentOutputs = new ArrayList<>();
		}
		
		OutputStream openOutput(String path, String sourcePath) throws IOException {
			//System.err.println("Writing "+sourcePath+" to "+path);
			if( "-".equals(path) ) {
				return System.out;
			} else {
				File f = new File(path);
				File parent = f.getParentFile();
				if( !parent.exists() ) parent.mkdirs();
				return new FileOutputStream(f);
			}
		}
		
		static String normalizeSourceFilename(String fn) {
			while( fn.startsWith("/") ) fn = fn.substring(1);
			while( fn.endsWith("/") ) fn = fn.substring(0, fn.length()-1);
			fn = fn.replace("..", ".dotdot");
			return fn;
		}
		
		static String destPathConcat(String destPath, String childName) {
			if( destPath.isEmpty() ) throw new RuntimeException("destPath.isEmpty()");
			if( "-".equals(destPath) ) return destPath;
			return destPath+"/"+childName;
		}
		
		@Override
		public void chunkMetadata(String k, String v) throws IOException {
			if( currentOutputs == null ) throw new RuntimeException("chunkMetadata(): currentOutputs == null");
			if( "filename".equals(k) ) {
				v = normalizeSourceFilename(v);
				for( ExtractionRule rule : extractionRules ) {
					if( v.equals(rule.sourcePath) ) {
						currentOutputs.add(openOutput(rule.destPath, v));
					} else if( ".".equals(rule.sourcePath) ) {
						// This one's a little special as it represents no prefix
						currentOutputs.add(openOutput(destPathConcat(rule.destPath, v), v));
					} else if( v.startsWith(rule.sourcePath+"/") ) {
						String suffix = v.substring(rule.sourcePath.length()+1);
						currentOutputs.add(openOutput(destPathConcat(rule.destPath, suffix), v));
					} else {
						// Debug
						//System.err.println("Ignoring "+v);
					}
				}
			}
		}
		
		@Override
		public void chunkData(byte[] data, int offset, int len) throws IOException {
			for( OutputStream os : this.currentOutputs ) {
				os.write(data, offset, len);
			}
		}
		
		@Override
		public void closeChunk() {
			if( currentOutputs == null ) {
				throw new RuntimeException("closeChunk(): currentOutputs null!");
			}
			for( OutputStream os : this.currentOutputs ) {
				if( os != System.out ) try {
					os.close();
				} catch( IOException e ) {
					System.err.println("Error closing output stream: "+e.getMessage());
				}
			}
			this.currentOutputs = null;
		}
		
		@Override
		public void close() {
			for( OutputStream os : this.currentOutputs ) {
				if( os != System.out ) try {
					os.close();
				} catch( IOException e ) {
					System.err.println("Error closing output stream: "+e.getMessage());
				}
			}
			this.currentOutputs = null;
		}
	}
	
	static class ListChunkHandler implements ChunkHandler {
		@Override
		public void parseWarning(String w) {
			System.err.println("warning: "+w);
		}
		
		@Override
		public void streamMetadata(String k, String v) {}
		
		boolean filenameFound = false;
		long bytesRead = 0;
		
		@Override
		public void openChunk() {
			filenameFound = false;
			bytesRead = 0;
		}
		
		@Override
		public void chunkMetadata(String k, String v) {
			if( "filename".equals(k) ) {
				System.out.println(v);
				filenameFound = true;
			}
		}
		
		@Override
		public void chunkData(byte[] data, int offset, int len) {
			bytesRead += len;
		}
		
		@Override
		public void closeChunk() {
			if( !filenameFound ) {
				parseWarning("Chunk encountered with no filename");
				//System.err.println("ListChunkHandler.closeChunk(): Chunk had no filename");
			}
			//System.err.println("ListChunkHandler.closeChunk(): Closed "+bytesRead+"-byte chunk");
		}
		
		@Override
		public void close() {}
	}
	
	static Pattern EXTRACT_PAT = Pattern.compile("^--extract:([^=]+)(?:=(.*))?$");
	
	// main should probably be split into 'parse arguments'
	// and 'do action' so we can better unit test each.
	
	public static int main(String[] args, int arg0) throws IOException {
		RunMode mode = RunMode.UNSET;
		String selfName = "almost-multipart-decoder";
		ArrayList<String> usageErrors = new ArrayList<>();
		ArrayList<ExtractionRule> extractionRules = new ArrayList<ExtractionRule>();
		
		for( int i=arg0; i<args.length; ++i) {
			final String arg = args[i];
			Matcher m;
			if ("--help".equals(arg) ) {
				mode = RunMode.HELP;
			} else if( "--extract".equals(arg) ) {
				mode = RunMode.EXTRACT;
				extractionRules.add(new ExtractionRule(".", "."));
			} else if( (m = EXTRACT_PAT.matcher(arg)).matches() ) {
				mode = RunMode.EXTRACT;
				String sourcePath = m.group(2);
				if( sourcePath == null || sourcePath.isEmpty() ) sourcePath = ".";
				extractionRules.add(new ExtractionRule(sourcePath, m.group(1)));
			} else if( "--list".equals(arg) ) {
				mode = RunMode.LIST;
			} else {
				usageErrors.add("Unrecognized argument: "+arg);
			}
		}
		
		if( mode == RunMode.UNSET ) {
			usageErrors.add("No action indicated");
		}
		if( !usageErrors.isEmpty() ) {
			for( String err : usageErrors ) {
				System.err.println(selfName+": Error: "+err);
			}
			System.err.println(selfName+": Try --help for help");
			return 1;
		}
		
		if( mode == RunMode.HELP ) {
			System.out.println(selfName+": Extract files from x-almost-multipart archives");
			System.out.println("Usage:");
			System.out.println("  decode --help     ; print this text");
			System.out.println("  decode --list     ; list names of contained files");
			System.out.println("  decode --extract  ; extract everything from <file>");
			System.out.println("                    ; equivalent to --extract:.=.");
			System.out.println("  decode --extract:<dest>=<source> ...");
			System.out.println("                    ; extract specific files or directories to specific destinations");
			System.out.println();
			System.out.println("For source paths, '.' means root of the archive.");
			System.out.println("For dest paths, '.' means current directory.");
			System.out.println("A destination of '-' means standard output.");
			System.out.println("Archive is always read from standard input.");
			System.out.println();
			System.out.println("Example: extract README.txt to standard output:");
			System.out.println("  "+selfName+" --extract:-=README.txt");
			System.out.println();
			System.out.println("Example: extract entire archive to directory 'xxx':");
			System.out.println("  "+selfName+" --extract:xxx=.");
			return 0;
		}
		
		ChunkHandler chunkHandler;
		switch( mode ) {
			case EXTRACT:
				chunkHandler = new ExtractionRuleChunkHandler(extractionRules);
				break;
			case LIST:
				chunkHandler = new ListChunkHandler();
				break;
			default:
				throw new RuntimeException("Shouldn't have gotten here");
		}
		
		try (Decoder decoder = new Decoder(chunkHandler)) {
			byte[] buffer = new byte[1024];
			int len;
			while( (len = System.in.read(buffer)) > 0) {
				decoder.input(buffer, 0, len);
			}
		}
		return 0;
	}
	
	public static void main(String[] args) throws IOException {
		System.exit(main(args, 0));
	}
}
