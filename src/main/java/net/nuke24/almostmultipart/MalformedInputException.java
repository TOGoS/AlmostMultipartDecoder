package net.nuke24.almostmultipart;

import java.io.IOException;

class MalformedInputException extends IOException {
	public MalformedInputException(String message) {
		super(message);
	}
	public MalformedInputException(Throwable cause) {
		super(cause);
	}
}
