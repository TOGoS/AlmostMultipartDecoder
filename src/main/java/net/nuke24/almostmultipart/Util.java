package net.nuke24.almostmultipart;

import java.io.UnsupportedEncodingException;

public class Util {
	public static byte[] utf8(String s) {
		try {
			return s.getBytes("UTF-8");
		} catch( UnsupportedEncodingException e ) {
			throw new RuntimeException(e);
		}
	}
	
	public static String utf8Decode(byte[] b) throws MalformedInputException {
		try {
			return new String(b, "UTF-8");
		} catch( UnsupportedEncodingException e ) {
			throw new MalformedInputException(e);
		}
	}
}
